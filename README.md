# calculo de poblacion de pelibueyes

Parametros de Manejo de población de pelibueyes a venta. (Simulación)

Las reglas: Un pelibuey es fertil a los 6 meses, la gestacion tarda 4 meses y usualmente es fecundada de inmediato. Usualmente paren un solo crío, el sexo es 50:50. Para la venta el sexo no es relevante, usualmente se venden a los 18 meses. Sin embargo es mejor vender primero los machos, porque no aportan al crecimiento de población. Pero no se pueden vender todos los machos, al menos debe quedar uno. Existe la meta de vender 4 pelibueyes por mes.

Usar parámetros para el manejo de la población, podria ayudar a hacer ajuste si las condiciones varian. Por decir la edad de fertilidad se incrementa por problemas de nutricion. Pero por otra parte si hay suficiente parametros podría sustituirse y cambiar a otra especie. Vacas, cerdos, gallinas.

Las preguntas que busca responder esta simulación son:
Cual es la población que debe alcanzar el rebaño para que sea posible vender esa cantidad meta? Cuanto tiempo se tarda en llegar a esa polación por crecimiento natural (es decir sin comprar animales)?

No tengo idea de lo que estoy haciendo.

La primera pregunta podría calcularla de diversas formas (Aritmetica, busqueda de metas). La segunda, es un poco más complicada (crecimiento poblacional). Sin embargo la simulación me ayuda a practicar la parte de programación orientada a objetos de python. Quien sabe... incluso podria terminar con gráficos de piramides/arboles de estratificación de edades y genero.

Hay un error de lógica que debo corregir... si la poblacion son las hembras más los machos... a la hora de vender deberia restar las hembras y los machos, para mantener el total de la población real y la disponibilidad de reproductores. No he incluido eliminar las instancias  vendidas para que el modelo no cresca al infinito deben eliminarse las instancias vendidas. Otra cosa, la venta debe apuntar a los animales de mayor edad.

Me queda la inquietud si se deberian manejar como individuos? o como grupos de individuos que comparten caracteristicas (edad, sexo, etc.)


